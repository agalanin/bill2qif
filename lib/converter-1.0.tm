############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2022 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require msgcat 1.5

package provide converter 1.0

namespace eval converter {

    proc convertSplit {data} {
        set res {}

        set quantity [dict get $data quantity]
        set note [dict get $data text]
        if {[dict exist $data unit]} {
            set unit [dict get $data unit]
            set note [format "%s (%.2f %s)" $note $quantity $unit]
        } else {
            if {$quantity != 1} {
                set note [msgcat::mc "%s (%.2f units)" $note $quantity]
            }
        }

        lappend res "S[dict get $data category]"
        lappend res "E$note"
        lappend res [format {$%.2f} [expr {-[dict get $data sum] / 100.0}]]

        join $res \n
    }

    proc convertBill {bill} {
        set res {}

        # bill header
        lappend res "!Type:CCard"
        lappend res "D[clock format [dict get $bill dateTime] -format {%d.%m.%y}]"
        lappend res [format {T%.2f} [expr {-[dict get $bill sum] / 100.0}]]
        lappend res "P[dict get $bill shopName]"
        # splits
        foreach split [lreverse [dict get $bill splits]] {
            lappend res [convertSplit $split]
        }
        lappend res "^"

        join $res \n
    }

    proc convertBillList {bills} {
        set res {}

        # file header
        lappend res "!Account"
        lappend res "NCredit card"
        lappend res "TCCard"
        lappend res "^"
        # bills
        foreach bill $bills {
            lappend res [convertBill $bill]
        }

        join $res \n
    }

    namespace export *

}

# vim: set ft=tcl:

