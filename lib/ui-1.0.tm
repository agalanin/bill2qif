############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2022 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require Tk 8.6
package require autoscroll
package require inifile 0.3
package require msgcat
package require struct::list 1.8
package require tablelist_tile

package require config 1.0
package require patterns 1.0
package require shops 1.0
package require uiutil 1.0

package provide ui 1.0

namespace eval ui {

    variable startButton
    variable endButton
    variable prevButton
    variable nextButton
    variable headerLabel

    variable shopWidget
    variable dateWidget
    variable sumWidget

    variable currentBill -1
    variable totalBills
    variable table

    variable categoryList {}
    variable patterns {}
    variable newPatterns {}
    variable shops {}

    variable shop ""
    variable origShop ""
    variable tin ""
    variable date ""
    variable sum ""

    variable bills {}

    variable lastOpenFile ""
    variable lastSaveFile ""

    proc initMainWindow {} {
        variable ::main::version
        variable table

        wm title . [mc "Bill converter %s" $version]
        wm protocol . WM_DELETE_WINDOW [namespace current]::onExit

        grid columnconfigure . 0 -weight 1
        grid rowconfigure . 2 -weight 1

        set w ""
        grid \
            [headerPanel $w.header] \
            -padx 0 -pady 0 \
            -sticky nswe
        grid \
            [descriptionPanel $w.description] \
            -padx 0 -pady 0 \
            -sticky nswe
        grid \
            [splitPanel $w.splits] \
            -padx 0 -pady 0 \
            -sticky nswe

        initShortcuts .

        focus $table
        after idle [list [namespace current]::gotoCell 0 category false]
    }

    proc initShortcuts {w} {
        variable prevButton
        variable nextButton
        variable table
        variable shopWidget

        foreach {key cmd} [list \
            <Shift-Control-Tab> "$prevButton invoke" \
            <ISO_Left_Tab>      "$prevButton invoke" \
            <Control-Tab>       "$nextButton invoke" \
            <Control-n>         "[namespace current]::gotoNextUncategorized" \
            <Control-l>         "focus $shopWidget; $shopWidget selection range 0 end" \
        ] {
            catch {
                bind $w $key "$cmd; break"
            }
            catch {
                bind [$table bodypath] $key "$cmd; break"
            }
        }
        catch {
            bind [$table bodypath] <Delete> [namespace current]::deleteCellContent
        }
        catch {
            bind [$table bodypath] <Control-v> "[namespace current]::cellInsert CLIPBOARD; break"
        }
        catch {
            bind [$table bodypath] <Shift-Insert> "[namespace current]::cellInsert CLIPBOARD; break"
        }
        catch {
            bind [$table bodypath] <Button-2> "[namespace current]::cellInsert PRIMARY"
        }
    }

    proc headerPanel {f} {
        variable startButton
        variable endButton
        variable prevButton
        variable nextButton
        variable headerLabel

        ttk::frame $f

        grid columnconfigure $f 2 -weight 1 -minsize 100
        grid rowconfigure $f 0 -weight 1

        grid \
            [set startButton [ttk::button $f.start \
                -text [mc "<<"] \
                -command [namespace current]::firstBill \
            ]] \
            [set prevButton [ttk::button $f.prev \
                -text [mc "<"] \
                -command [namespace current]::prevBill \
            ]] \
            [set headerLabel [ttk::label $f.label \
                -anchor center \
            ]] \
            [set nextButton [ttk::button $f.next \
                -text [mc ">"] \
                -command [namespace current]::nextBill \
            ]] \
            [set endButton [ttk::button $f.end \
                -text [mc ">>"] \
                -command [namespace current]::lastBill \
            ]] \
            -padx 2 -pady 2 \
            -sticky nswe

        return $f
    }

    proc updateHeader {} {
        variable startButton
        variable endButton
        variable prevButton
        variable nextButton
        variable headerLabel
        variable currentBill
        variable totalBills

        $headerLabel configure \
            -text [mc "Bill %d of %d" $currentBill $totalBills]

        if {$currentBill == 1} {
            $startButton configure -state disabled
            $prevButton configure -state disabled
        } else {
            $startButton configure -state normal
            $prevButton configure -state normal
        }
        if {$currentBill == $totalBills} {
            $endButton configure -state disabled
            $nextButton configure -state disabled
        } else {
            $endButton configure -state normal
            $nextButton configure -state normal
        }
    }

    proc descriptionPanel {f} {
        variable shopWidget
        variable dateWidget
        variable sumWidget

        ttk::frame $f

        grid columnconfigure $f 1 -weight 1

        grid \
            [ttk::label $f.shopLabel \
                -text [mc "Shop: "] \
            ] \
            [set shopWidget [ttk::entry $f.shop \
                -textvariable [namespace current]::shop \
            ]] \
            -padx 2 -pady 2 \
            -sticky nswe
        grid \
            [ttk::label $f.dateLabel \
                -text [mc "Date: "] \
            ] \
            [set dateWidget [ttk::label $f.date \
                -textvariable [namespace current]::date \
            ]] \
            -padx 2 -pady 2 \
            -sticky nswe
        grid \
            [ttk::label $f.sumLabel \
                -text [mc "Sum: "] \
            ] \
            [set sumWidget [ttk::label $f.sum \
                -textvariable [namespace current]::sum \
            ]] \
            -padx 2 -pady 2 \
            -sticky nswe

        return $f
    }

    proc splitPanel {f} {
        variable table

        ttk::frame $f

        grid columnconfigure $f 0 -weight 1
        grid rowconfigure $f 0 -weight 1

        grid \
            [set table [tablelist::tablelist $f.table \
                -listvariable [namespace current]::splits \
                -selecttype cell \
                -columns [list \
                    0   [mc "Name"] \
                    0   [mc "Sum"] \
                    0   [mc "Category"] \
                    0   [mc "Pattern"] \
                    0   quantity \
                    0   unit
                ] \
                -editstartcommand [namespace current]::editStart \
                -editendcommand [namespace current]::editEnd \
                -xscrollcommand [list $f.hscroll set] \
                -yscrollcommand [list $f.vscroll set] \
            ]] \
            [ttk::scrollbar $f.vscroll \
                -orient vertical \
                -command [list $f.table yview] \
            ] \
            -padx 0 -pady 0 \
            -sticky nswe
        grid \
            [ttk::scrollbar $f.hscroll \
                -orient horizontal \
                -command [list $f.table xview] \
            ] \
            x \
            -padx 0 -pady 0 \
            -sticky nswe

        $table columnconfigure 0 \
            -name name
        $table columnconfigure 1 \
            -name sum
        $table columnconfigure 2 \
            -name category \
            -editable true \
            -editwindow ttk::combobox
        $table columnconfigure 3 \
            -name pattern \
            -editable true \
            -editwindow ttk::entry
        $table columnconfigure 4 \
            -name quantity \
            -hide yes
        $table columnconfigure 5 \
            -name unit \
            -hide yes

        autoscroll::autoscroll $f.hscroll
        autoscroll::autoscroll $f.vscroll

        return $f
    }

    proc editStart {table row column value} {
        variable categoryList

        switch [$table columncget $column -name] {
            category {
                [$table editwinpath] configure -values $categoryList
                return $value
            }
            pattern {
                if {[$table cellcget $row,category -text] eq ""} {
                    # don't edit pattern if category is not set
                    $table cancelediting
                } elseif {$value eq ""} {
                    after idle [list [namespace current]::setSuggestedPattern \
                        [$table editwinpath] \
                        [$table cellcget $row,name -text] \
                    ]
                }
                return $value
            }
            default {
                error "unexpected program flow"
            }
        }
    }

    proc editEnd {table row column value} {
        set col [$table columncget $column -name]
        set valSet [expr {$value ne ""}]
        set name [$table cellcget $row,name -text]
        set category [$table cellcget $row,category -text]
        set pattern  [$table cellcget $row,pattern  -text]

        if {[$table cellcget $row,$column -text] ne ""} {
            if {$valSet} {
                set action reset
            } else {
                set action unset
            }
        } else {
            if {$valSet} {
                set action set
            } else {
                # -forceeditendcommand is false by default and clipboard
                # operations calls this method only for non-empty content
                error "double unset should never happen"
            }
        }

        switch $col {
            category {
                if {$pattern ne ""} {
                    $table cellconfigure $row,pattern -text ""
                    if {[removePattern $pattern]} {
                        after idle [list [namespace current]::applyAllBills]
                    } else {
                        after idle [namespace current]::persistCurrentBill
                    }
                } else {
                    after idle [namespace current]::persistCurrentBill
                }
            }
            pattern {
                # category is always set because [editStart] forbids pattern
                # editing if category is empty
                switch $action {
                    set {
                        if {![checkPatternCompatibility $value $name]} {
                            $table rejectinput
                            return
                        }
                        if {![validatePattern $value]} {
                            $table rejectinput
                            return
                        }
                        addPattern $value $category
                        after idle [list [namespace current]::applyAllBills]
                    }
                    reset {
                        if {![checkPatternCompatibility $value $name]} {
                            $table rejectinput
                        } else {
                            if {[replacePattern $pattern $value $category]} {
                                after idle [list [namespace current]::applyAllBills]
                            } else {
                                $table rejectinput
                            }
                        }
                    }
                    unset {
                        if {[removePattern $pattern]} {
                            after idle [list [namespace current]::applyAllBills]
                        } else {
                            after idle [namespace current]::persistCurrentBill
                        }
                    }
                }
            }
            default {
                error "column $column is not editable!"
            }
        }

        return $value
    }

    proc displaySplits {index} {
        variable bills
        variable splits

        set data [lindex $bills $index-1]
        set splits [uiutil::splitsToList [dict get $data splits]]
    }

    proc displayBill {index} {
        variable shop
        variable origShop
        variable tin
        variable date
        variable sum
        variable bills
        variable currentBill
        variable table

        if {$currentBill != $index} {
            $table cancelediting
            
            if {$currentBill >= 0} {
                persistShopName
                applyPatterns
            }
        }

        set currentBill $index
        set data [lindex $bills $index-1]

        set shop [dict get $data shopName]
        set origShop [dict get $data shopName]
        set tin [dict get $data TIN]
        set date [clock format [dict get $data dateTime] -format {%d.%m.%Y %H:%M}]
        set sum [format %.2f [expr {[dict get $data sum] / 100.0}]]

        updateHeader
        displaySplits $index
        gotoCell 0 category false
    }

    proc firstBill {} {
        displayBill 1
    }

    proc lastBill {} {
        variable bills
        displayBill [llength $bills]
    }

    proc nextBill {} {
        variable currentBill
        displayBill [expr {$currentBill + 1}]
    }

    proc prevBill {} {
        variable currentBill
        displayBill [expr {$currentBill - 1}]
    }

    proc onExit {} {
        variable bills

        persistShopName
        applyPatterns

        set unmatched [::processor::listUnmatched $bills]
        if {[llength $unmatched] != 0} {
            if {[llength $unmatched] > 5} {
                set unmatched [concat [lrange $unmatched 0 4] ...]
            }
            set detail [join $unmatched \n]
            set answer [tk_messageBox \
                -parent . \
                -title [mc "Bill converter"] \
                -message [mc "There are unassigned splits. Do you want to exit without saving?"] \
                -detail $detail \
                -icon warning \
                -type yesno \
                -default no \
            ]
            if {$answer eq "yes"} {
                exit 1
            }
        } else {
            set answer [tk_messageBox \
                -parent . \
                -title [mc "Bill converter"] \
                -message [mc "Do you want to save result to QIF file?"] \
                -icon question \
                -type yesnocancel \
                -default yes \
            ]
            switch -- $answer {
                yes {
                    if {[saveFile]} {
                        saveConfig
                        exit 0
                    }
                }
                no {
                    exit 1
                }
                cancel {
                    # do nothing
                }
            }
        }
    }

    proc errorMessage {message detail} {
        tk_messageBox \
            -parent . \
            -title [mc "Split editor"] \
            -message $message \
            -detail $detail \
            -icon error
    }

    proc allPatterns {} {
        variable patterns
        variable newPatterns

        dict merge $patterns $newPatterns
    }

    proc checkPatternCompatibility {pattern string} {
        if {![::patterns::isMatches $pattern $string]} {
            errorMessage \
                [mc "Incompatible pattern"] \
                [mc "Split name '%s' is incompatible with entered pattern '%s'" $string $pattern]
            return false
        } else {
            return true
        }
    }

    proc validatePattern {pattern} {
        # validate against available patterns
        set res [::patterns::findConflictingPattern $pattern [allPatterns]]
        if {[llength $res] == 0} {
            return true
        }
        # display error message
        lassign $res pat cat
        errorMessage \
            [mc "Conflicting pattern"] \
            [mc "Pattern '%s' conflicts with pattern '%s' for category '%s'" $pattern $pat $cat]

        return false
    }

    proc addPattern {pattern category} {
        variable newPatterns

        dict set newPatterns $pattern $category
    }

    proc applyPatterns {} {
        variable bills
        variable currentBill
        variable shops

        set bills [processor::applyRules [allPatterns] $shops $bills]
    }

    proc persistShopName {} {
        variable bills
        variable currentBill
        variable tin
        variable shop
        variable origShop
        variable shops

        if {$shop ne $origShop} {
            ::shops::setShopName shops $tin $shop
        }

        set new [lindex $bills [expr {$currentBill - 1}]]
        dict set new shopName $shop
        lset bills [expr {$currentBill - 1}] $new
    }

    proc persistCurrentBill {} {
        variable bills
        variable currentBill
        variable splits

        set new [lindex $bills [expr {$currentBill - 1}]]
        dict set new splits [uiutil::listToSplits $splits]
        lset bills [expr {$currentBill - 1}] $new

        refreshCategories
    }

    proc refreshCategories {} {
        variable bills
        variable categoryList
        variable patterns

        set categoryList [lsort -unique [concat \
            [dict values $patterns] \
            [processor::listCategories $bills] \
        ]]
    }

    proc removePattern {pattern} {
        variable newPatterns

        if {![dict exists $newPatterns $pattern]} {
            # don't remove pattern from the saved patterns list
            return false
        }

        set newPatterns [dict remove $newPatterns $pattern]
        return true
    }

    proc replacePattern {oldPattern newPattern category} {
        variable newPatterns

        set newPatternsBkp $newPatterns
        if {[dict exists $newPatterns $oldPattern]} {
            set newPatterns [dict remove $newPatterns $oldPattern]
        }
        if {![validatePattern $newPattern]} {
            set newPatterns $newPatternsBkp
            return false
        }
        dict set newPatterns $newPattern $category
        return true
    }

    proc applyAllBills {} {
        variable currentBill

        persistCurrentBill
        applyPatterns
        displaySplits $currentBill
    }

    proc saveFile {} {
        variable outFile
        variable bills

        control::do {
            if {$outFile ne ""} {
                set fname $outFile
            } else {
                set fname [getSaveFileName]
                if {$fname eq ""} {
                    return false
                }
            }

            try {
                set out [open $fname w]
                try {
                    puts $out [converter::convertBillList $bills]
                } finally {
                    close $out
                }
                return true
            } trap POSIX {err opts} {
                tk_messageBox \
                    -parent . \
                    -title [mc "Bill converter"] \
                    -message [mc "File save error!"] \
                    -detail $err \
                    -icon error
            }
        } while {$outFile eq ""}

        return false
    }

    proc gotoCell {row col edit} {
        variable table

        $table cellselection clear active
        $table activatecell $row,$col
        $table cellselection set $row,$col
        if {$edit} {
            $table editcell $row,$col
        }
    }

    proc gotoNextUncategorized {} {
        variable table
        variable splits
        variable currentBill
        variable bills

        $table cancelediting

        set row [lindex [split [$table cellindex active] ,] 0]
        if {$row eq ""} {
            set row 0
        }

        # search for uncategorized split in current bill from the
        # current row
        set new [uiutil::firstUncategorizedInList $splits $row]
        if {$new >= 0} {
            gotoCell $new category true
            return
        }
        # search in the other bills
        set n [llength $bills]
        for {set i [expr {$currentBill + 1}]} {$i < $currentBill + $n} {incr i} {
            set idx [expr {($i - 1) % $n}]
            set bill [lindex $bills $idx]
            set new [uiutil::firstUncategorizedInSplits [dict get $bill splits]]
            if {$new >= 0} {
                displayBill [expr {$idx + 1}]
                gotoCell $new category true
                return
            }
        }
        # search for uncategorized split in current bill from the
        # beginning
        set new [uiutil::firstUncategorizedInList $splits 0]
        if {$new >= 0} {
            gotoCell $new category true
            return
        }
    }

    proc deleteCellContent {} {
        variable table

        lassign [split [$table cellindex active] ,] row column
        if {$column eq ""} {
            return
        }
        set col [$table columncget $column -name]

        set category [$table cellcget $row,category -text]
        set pattern  [$table cellcget $row,pattern  -text]

        switch $col {
            category {
                if {$category eq ""} {
                    # already unset
                    return
                }
                $table cellconfigure $row,category -text ""
                if {$pattern ne ""} {
                    $table cellconfigure $row,pattern -text ""
                    if {[removePattern $pattern]} {
                        applyAllBills
                    } else {
                        persistCurrentBill
                    }
                } else {
                    persistCurrentBill
                }
            }
            pattern {
                if {$pattern eq ""} {
                    # already unset
                    return
                }
                $table cellconfigure $row,pattern -text ""
                if {[removePattern $pattern]} {
                    applyAllBills
                } else {
                    persistCurrentBill
                }
            }
        }
    }

    proc cellInsert {selection} {
        variable table

        if {[catch {selection get -selection $selection} content] || $content eq ""} {
            return
        }

        lassign [split [$table cellindex active] ,] row column
        if {$column eq ""} {
            return
        }
        set col [$table columncget $column -name]

        switch $col {
            category {
                editEnd $table $row $column $content
                $table cellconfigure $row,$column -text $content
            }
            pattern {
                #TODO
            }
        }
    }

    proc setSuggestedPattern {w value} {
        $w insert end $value
        $w selection range 0 end
    }

    proc saveConfig {} {
        variable configFile
        variable shops

        while {true} {
            try {
                file mkdir [file dirname $configFile]
                ::config::saveToFile $configFile [allPatterns] $shops
                return
            } trap POSIX err {
                if {[tk_messageBox \
                    -parent . \
                    -title [mc "Bill converter"] \
                    -message [mc "Unable to save config file!"] \
                    -detail $err \
                    -icon error \
                    -type retrycancel \
                ] ne "retry"} {
                    return
                }
            }
        }
    }

    proc getOpenFileName {} {
        variable lastOpenFile

        set lastOpenFile [tk_getOpenFile \
            -parent . \
            -filetypes [list \
                [list [mc "JSON files"] {.json}] \
            ] \
            -initialdir [file dirname $lastOpenFile] \
            -initialfile $lastOpenFile \
            -title [mc "Open bill file"] \
        ]
    }

    proc getSaveFileName {} {
        variable lastOpenFile
        variable lastSaveFile

        if {$lastSaveFile eq ""} {
            set root [file rootname $lastOpenFile]
            if {$root eq ""} {
                set root _
            }
            set lastSaveFile "$root.qif"
        }

        set lastSaveFile [tk_getSaveFile \
            -parent . \
            -filetypes [list \
                [list [mc "Quicken Interchange Format files"] {.qif}] \
            ] \
            -initialdir [file dirname $lastSaveFile] \
            -initialfile [file tail $lastSaveFile] \
            -title [mc "Save QIF file"] \
            -defaultextension .qif \
        ]
    }

    proc setTitle {fileName} {
        variable ::main::version

        wm title . [mc "Bill converter %s - %s" $version [file tail $fileName]]
    }

    proc displayWarning {msg} {
        tk_messageBox \
            -parent . \
            -title [mc "Bill converter"] \
            -message [mc "Malformed bill"] \
            -detail $msg \
            -icon warning
    }

    namespace export *
}

# vim: set ft=tcl:

