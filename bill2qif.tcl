#!/bin/sh
############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2021 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################
# \
exec tclsh8.6 "$0" "$@"

package require Tcl 8.6
package require cmdline
package require control
package require msgcat

namespace eval main {
    variable version 1.1.0
}

# if there are no starkit module then set mode to 'unwrapped'
if {[catch {package require starkit}]} {
    namespace eval starkit {
        variable mode unwrapped
    }
} else {
    starkit::startup
}

set dir [file dirname [file normalize [info script]]]
if {$starkit::mode eq "starpack"} {
    ::tcl::tm::path add [file join $dir tm]
} else {
    set libdir [file join $dir lib]
    if {![file isdirectory $libdir]} {
        set libdir [file join [file dirname $dir] lib bill2qif]
    }
    ::tcl::tm::path add $libdir
}

set msgdir [file join $dir msgs]
if {![file isdirectory $msgdir]} {
    set msgdir [file join [file dirname $dir] share bill2qif msgs]
}
::msgcat::mcload $msgdir

namespace import msgcat::mc

package require bills 1.0
package require config 1.0
package require converter 1.0
package require processor 1.0

############################################################################
# FUNCTIONS
############################################################################

namespace eval main {

variable mode
variable cfgFile
variable inFile
variable outFile

proc parseCmdline {} {
    variable version

    global argv

    variable mode
    variable cfgFile
    variable inFile
    variable outFile

    set options [list \
        [list interactive           [mc "Run in interactive mode (default)"]] \
        [list automated             [mc "Run in automated mode, exit on unknown item class"]] \
        [list config.arg "$cfgFile" [mc "Config file name"]] \
        [list version               [mc "Print version information and exit"]] \
    ]
    if {$::tcl_platform(platform) eq "windows"} {
        lappend options \
            [list forcestdout       [mc "Force printing messages to standard output/error channels"]]
    }
    set usage [mc "\[options] \[infile] \[outfile]\noptions:"]

    set origArgv $argv
    try {
        array set params [cmdline::getoptions argv $options $usage]
    } trap {CMDLINE USAGE} {msg opts} {
        initSystemSpecificMessages [expr {"-forcestdout" in $origArgv}]

        printError $msg
        return 0
    }

    set forceStdout false
    if {$::tcl_platform(platform) eq "windows"} {
        set forceStdout $params(forcestdout)
    }
    initSystemSpecificMessages $forceStdout

    if {$params(version)} {
        printInfo \
            [mc "Bill to QIF (Quicken Interchange Format) converter"] \
            [mc "Version: %s\nLicense: GPLv3" $version]
        return 0
    }

    if {$params(automated) && $params(interactive)} {
        printError \
            [mc "Only one mode can be selected!"] \
            [cmdline::usage $options $usage]
        return 1
    }

    if {[llength $argv] > 2} {
        printError [cmdline::usage $options $usage]
        return 1
    }

    if {$params(automated)} {
        set mode automated
    } else {
        set mode interactive
    }
    set cfgFile $params(config)

    lassign $argv inFile outFile

    return ""
}

proc readConfig {patternsVar shopsVar} {
    variable cfgFile
    upvar $patternsVar patterns
    upvar $shopsVar shops

    try {
        # input file parsing
        if {[file exists $cfgFile]} {
            lassign [::config::readFromFile $cfgFile] patterns shops
        } else {
            set patterns {}
            set shops {}
        }
        # input file validation
        set res [lassign [patterns::validateList $patterns] type]
        switch $type {
            "" {
                # do nothing
            }
            duplicate {
                lassign $res pattern
                throw {BILL2QIF LOGIC} [mc "duplicate pattern `%s'" $pattern]
            }
            conflict {
                lassign $res pat1 cat1 pat2 cat2
                throw {BILL2QIF LOGIC} [mc "conflicting patterns `%s' for `%s' and `%s' for `%s'" $pat1 $cat1 $pat2 $cat2]
            }
            empty {
                lassign $res pattern
                throw {BILL2QIF LOGIC} [mc "empty category for pattern `%s'" $pattern]
            }
            default {
                throw {BILL2QIF LOGIC} "unhandled case: $type"
            }
        }
    } trap POSIX {err opts} {
        errorHandler [mc "config file load failed"] $err $opts
        return false
    } trap {BILL2QIF LOGIC} {err opts} {
        errorHandler [mc "config file error"] $err $opts
        return false
    }

    return true
}

proc readInputFile {inFile billsVar} {
    upvar $billsVar bills

    try {
        if {$inFile eq ""} {
            set in stdin
        } else {
            set in [open $inFile r]
        }
        try {
            fconfigure $in -encoding utf-8
            set bills [bills::parseData [read $in]]
        } finally {
            close $in
        }
    } trap POSIX {err opts} {
        errorHandler [mc "input file read error"] $err $opts
        return false
    } trap {BILL2QIF INPUTFORMAT} {err opts} {
        errorHandler [mc "input file format error"] $err $opts
        return false
    } trap {BILL2QIF LOGIC} {err opts} {
        errorHandler [mc "input file error"] $err $opts
        return false
    }
    return true
}

proc saveOutputFile {outFile bills} {
    try {
        if {$outFile eq ""} {
            set out stdout
        } else {
            set out [open $outFile w]
        }
        try {
            puts $out [converter::convertBillList $bills]
        } finally {
            close $out
        }
    } trap POSIX {err opts} {
        errorHandler [mc "output file save error"] $err $opts
        return false
    }
    return true
}

proc initSystemSpecificDirs {} {
    variable cfgFile

    set basename "bill2qif.conf"
    switch $::tcl_platform(platform) {
        unix {
            if {[info exists ::env(XDG_CONFIG_HOME)]} {
                set dir $::env(XDG_CONFIG_HOME)
            } else {
                set dir [file join $::env(HOME) .config]
            }
        }
        windows {
            if {[info exists ::env(APPDATA)]} {
                set dir $::env(APPDATA)
            } else {
                set dir [file join $::env(HOME) AppData]
            }
        }
        default {
            error "unsupported platform $::tcl_platform(platform)"
        }
    }
    set cfgFile [file join $dir $basename]
}

proc initConsoleMessages {} {
    proc printInfo {args} {
        foreach line $args {
            puts stdout $line
        }
    }

    proc printWarning {args} {
        foreach line $args {
            puts stderr $line
        }
    }

    proc printError {args} {
        printWarning {*}$args
    }
}

proc initGUIMessages {} {
    package require Tk 8.6

    proc printInfo {args} {
        set args [split [join $args \n] \n]
        wm withdraw .
        tk_messageBox \
            -title [mc "Bill converter"] \
            -message [lindex $args 0] \
            -detail [join [lrange $args 1 end] \n] \
            -icon info
    }

    proc printWarning {args} {
        set args [join $args \n]
        wm withdraw .
        tk_messageBox \
            -title [mc "Bill converter"] \
            -message [mc "Malformed bill"] \
            -detail $args \
            -icon warning
    }

    proc printError {args} {
        set args [split [join $args \n] \n]
        wm withdraw .
        tk_messageBox \
            -title [mc "Bill converter"] \
            -message [lindex $args 0] \
            -detail [join [lrange $args 1 end] \n] \
            -icon error
    }
}

proc initSystemSpecificMessages {forceStdout} {
    switch $::tcl_platform(platform) {
        unix {
            initConsoleMessages
        }
        windows {
            if {$forceStdout} {
                initConsoleMessages
            } else {
                initGUIMessages
            }
        }
        default {
            error "unsupported platform $::tcl_platform(platform)"
        }
    }
}

proc main {} {
    variable mode
    variable cfgFile
    variable inFile
    variable outFile

    initSystemSpecificDirs

    if {[set code [parseCmdline]] ne ""} {
        return $code
    }

    switch $mode {
        automated {
            proc errorHandler {prefix err opts} {
                puts stderr "$prefix: $err"
            }
        }
        interactive {
            package require ui 1.0

            ui::initMainWindow
            set ::ui::configFile $cfgFile
            tkwait visibility .

            proc errorHandler {prefix err opts} {
                tk_messageBox \
                    -parent . \
                    -title [mc "Bill converter"] \
                    -message $prefix \
                    -detail $err \
                    -icon error
            }
        }
    }

    if {![readConfig patterns shops]} {
        return 1
    }

    switch $mode {
        automated {
            if {![readInputFile $inFile bills]} {
                return 1
            }
            # validation
            set warning [bills::validate $bills]
            if {$warning ne ""} {
                printWarning $warning
            }
        }
        interactive {
            if {$inFile ne ""} {
                if {![readInputFile $inFile bills]} {
                    return 1
                }
                set ::ui::lastOpenFile $inFile
            } else {
                control::do {
                    set inFile [ui::getOpenFileName]
                    if {$inFile eq ""} {
                        return 0
                    }
                } while {![readInputFile $inFile bills]}
            }
            ui::setTitle $inFile
        }
    }

    # initial classification
    set processed [processor::applyRules $patterns $shops $bills]

    switch $mode {
        automated {
            if {[llength [set unmatched [processor::listUnmatched $processed]]] > 0} {
                foreach name $unmatched {
                    printError [mc "unmatched split: %s" $name]
                }
                return 2
            }

            if {![saveOutputFile $outFile $processed]} {
                return 1
            }

            return 0
        }
        interactive {
            set ::ui::patterns $patterns
            set ::ui::shops $shops
            set ::ui::bills $processed
            set ::ui::totalBills [llength $processed]
            set ::ui::outFile $outFile

            ::ui::displayBill 1
            ::ui::refreshCategories

            # validation
            set warning [bills::validate $bills]
            if {$warning ne ""} {
                ui::displayWarning $warning
            }

            return
        }
    }
}

}

############################################################################
# MAIN
############################################################################

if {[set code [main::main]] ne ""} {
    exit $code
}
