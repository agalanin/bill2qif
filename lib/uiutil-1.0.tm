############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2022 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require struct::list 1.8

package provide uiutil 1.0

namespace eval uiutil {

    proc splitsToList {splits} {
        struct::list mapfor split $splits {
            if {[dict exists $split unit]} {
                set unit [dict get $split unit]
            } else {
                set unit ""
            }
            list \
                [dict get $split text] \
                [format %.2f [expr {[dict get $split sum] / 100.0}]] \
                [dict get $split category] \
                [dict get $split pattern] \
                [dict get $split quantity] \
                $unit
        }
    }

    proc listToSplits {list} {
        struct::list mapfor item $list {
            lassign $item text sum category pattern quantity unit
            set manual [lindex {false true} [expr {$pattern eq "" && $category ne ""}]]
            if {$unit ne ""} {
                set unitItem [list unit $unit]
            } else {
                set unitItem {}
            }
            dict create \
                text        $text       \
                sum         [expr {round($sum * 100.0)}] \
                manual      $manual     \
                category    $category   \
                pattern     $pattern    \
                quantity    $quantity   \
                {*}$unitItem
        }
    }

    proc firstUncategorizedInList {list index} {
        set i 0
        foreach item $list {
            if {$i < $index} {
                incr i
                continue
            }

            if {[lindex $item 2] eq ""} {
                return $i
            }

            incr i
        }
        return -1
    }

    proc firstUncategorizedInSplits {splits} {
        set i 0
        foreach split $splits {
            if {[dict get $split category] eq ""} {
                return $i
            }
            incr i
        }
        return -1
    }

    namespace export *
}

# vim: set ft=tcl:

