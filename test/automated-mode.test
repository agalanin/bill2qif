package require Tcl 8.6
package require tcltest 2.3
package require struct::list 1.8

::tcltest::configure {*}$argv
::tcl::tm::path add [file join [file dirname [file normalize [info script]]] .. lib]

package require patterns

namespace eval ::automated-mode::test {
    namespace import ::tcltest::*

    source [file join [file dirname [info script]] common.tcl]

    variable main bill2qif.tcl
    variable exename [list \
        [info nameofexecutable] \
        [file normalize [file join [file dirname [info script]] .. $main]] \
    ]
    variable cfgFile ""

    proc cleanup {} {
        variable cfgFile

        file delete -force $cfgFile
    }

    proc cleanupAll {} {
        variable cfgFile
        variable inFile
        variable outFile

        file delete -force $cfgFile $inFile $outFile
    }

    proc config {content} {
        variable cfgFile

        set f [file tempfile cfgFile]
        puts $f {
            [config]
            format_version=1.0
        }
        puts $f $content
        close $f
    }

    proc run {args} {
        variable exename
        variable cfgFile

        set ::env(LC_ALL) C
        if {$::tcl_platform(platform) eq "windows"} {
            set args [list -forcestdout {*}$args]
        }
        exec -- {*}$exename -config $cfgFile {*}$args
    }

    proc outfile {data} {
        join [struct::list mapfor line [split [string trim $data] \n] {
            string trim $line
        }] \n
    }

    test usage "Usage test" -cleanup cleanup -body {
        config {}
        run -help 2>@1
    } -match glob -result {bill2qif \[options] \[infile] \[outfile]
options:*}

    test version "Print version information" -cleanup cleanup -body {
        config {}
        run -version
    } -match glob -result [outfile {
        Bill to QIF (Quicken Interchange Format) converter
        Version: *
    }]

    test config-validation-conflict "Config file validation (conflict)" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER*    = Cat
            *LIT*       = Foo
        }
        run -automated << [dataFile single.json]
    } -returnCodes error -result [outfile {
        config file error: conflicting patterns `*LITTER*' for `Cat' and `*LIT*' for `Foo'
    }]

    test config-validation-empty "Config file validation (empty category)" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER* =
        }
        run -automated << [dataFile single.json]
    } -returnCodes error -result [outfile {
        config file error: empty category for pattern `*LITTER*'
    }]

    test single-bill-unmatched-all "Unsucessful conversion of single bill file (all unmatched)" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile single.json]
    } -returnCodes error -result [outfile {
        unmatched split: CAT LITTER
        unmatched split: CABBAGE
        unmatched split: CORNCOB
        unmatched split: RED POTATOES
    }]

    test single-bill-unmatched-some "Unsucessful conversion of single bill file (some unmatched)" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER*    = Cat
            CABBAGE*    = Food
            CORNCOB*    = Food
        }
        run -automated << [dataFile single.json]
    } -returnCodes error -result [outfile {
        unmatched split: RED POTATOES
    }]

    test single-bill-success "Sucessful conversion of single bill file" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER*    = Cat
            CABBAGE*    = Food
            CORNCOB*    = Food
            *POTATO*    = Food
        }
        run -automated << [dataFile single.json]
    } -result [outfile {
        !Account
        NCredit card
        TCCard
        ^
        !Type:CCard
        D17.09.17
        T-520.21
        PHorns'n'Hoofs Ltd
        SFood
        ERED POTATOES (1.22 units)
        $-22.82
        SFood
        ECORNCOB (4.00 units)
        $-102.60
        SFood
        ECABBAGE (3.52 units)
        $-39.79
        SCat
        ECAT LITTER
        $-355.00
        ^
    }]

    test single-bill-success-file "Sucessful conversion of single bill file (file output)" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER*    = Cat
            CABBAGE*    = Food
            CORNCOB*    = Food
            *POTATO*    = Food
        }
        close [file tempfile outFile]
        try {
            run -automated "" $outFile << [dataFile single.json]
            set data [viewFile $outFile]
        } finally {
            file delete -force $outFile
        }
        set data
    } -result [outfile {
        !Account
        NCredit card
        TCCard
        ^
        !Type:CCard
        D17.09.17
        T-520.21
        PHorns'n'Hoofs Ltd
        SFood
        ERED POTATOES (1.22 units)
        $-22.82
        SFood
        ECORNCOB (4.00 units)
        $-102.60
        SFood
        ECABBAGE (3.52 units)
        $-39.79
        SCat
        ECAT LITTER
        $-355.00
        ^
    }]

    test multiple-bill-success "Sucessful conversion of multiple bill file" -cleanup cleanup -body {
        config {
            [patterns]
            *LITTER*    = Cat
            SALAD*      = Food
            *BURGER*    = Food
            LEMONADE*   = Food
        }
        run -automated << [dataFile multiple.json]
    } -result [outfile {
        !Account
        NCredit card
        TCCard
        ^
        !Type:CCard
        D26.04.17
        T-37.31
        PSTORE LTD
        SFood
        EDOUBLE BURGER
        $-3.00
        SFood
        ESALAD
        $-34.31
        ^
        !Type:CCard
        D26.04.17
        T-95.32
        PSTORE 2
        SFood
        ELEMONADE 2
        $-47.66
        SFood
        ELEMONADE 1
        $-47.66
        ^
    }]

    test units-success "Sucessful conversion of single bill file with units" -cleanup cleanup -body {
        config {
            [patterns]
            *powder*    = Household goods
            *juice*     = Food
            *apple*     = Food
        }
        run -automated << [dataFile units.json]
    } -result [outfile {
        !Account
        NCredit card
        TCCard
        ^
        !Type:CCard
        D02.01.21
        T-768.69
        PDelivery Service LLC
        SFood
        Eapples (1.00 kg)
        $-109.99
        SFood
        Eorange juice (2.00 units)
        $-199.80
        SHousehold goods
        Ewashing powder (1.00 pcs)
        $-459.00
        ^
    }]

    test read-nonexistent-json "Error case: read nonexistent JSON file" -cleanup cleanup -body {
        config {}
        set f [file tempfile inFile]
        close $f
        file delete $inFile
        run -automated $inFile
    } -returnCodes error -match glob -result {input file read error: couldn't open *: no such file or directory}

    test read-empty-json "Error case: read empty JSON file" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile empty.json]
    } -returnCodes error -result {input file format error: unexpected token "END" at position 0; expecting VALUE}

    test read-bad-json "Error case: read bad JSON file" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile bad-json.json]
    } -returnCodes error -result "input file format error: unexpected token \"\}\" at position 65; expecting VALUE"

    test read-empty-list-json "Error case: empty bill list in JSON file" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile empty-list.json]
    } -returnCodes error -result {input file error: empty bill list}

    test read-empty-bill-json "Error case: empty bill in JSON file" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile empty-bill.json]
    } -returnCodes error -result {input file error: bill 1: unsupported bill format}

    test read-empty-bill-2-json "Error case: empty bill in JSON file-2" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile empty-bill-2.json]
    } -returnCodes error -result {input file error: key "dateTime" not known in dictionary}

    test read-incomplete-split-json "Error case: incomplete split in JSON file" -cleanup cleanup -body {
        config {}
        run -automated << [dataFile incomplete-split.json]
    } -returnCodes error -result {input file error: bill 1: split 2: key "name" not known in dictionary}

    test save-qif-error "Error case: output file save error" -constraints unix -cleanup cleanupAll -body {
        config {
            [patterns]
            *LITTER*    = Cat
            *CABBAGE*   = Food
            *POTATO*    = Food
            CORNCOB*    = Food
        }

        set f [file tempfile inFile]
        puts $f [dataFile single.json]
        close $f

        set f [file tempfile outFile]
        close $f
        file attributes $outFile -permissions 00000

        run -automated $inFile $outFile
    } -returnCodes error -match glob -result {output file save error: couldn't open *: permission denied}

    test validate-bill "Warning: wrong total sum" -cleanup cleanupAll -body {
        config {
            [patterns]
            *=All
        }
        run -automated "" $outFile << [dataFile bad-sum.json]
    } -returnCodes error -result {bill 1: total sum 156.55 differs with split sum 56.55}

    cleanupTests
}

namespace delete ::automated-mode::test

# vim: set ft=tcl:
