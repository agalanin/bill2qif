#!/bin/sh
# \
exec tclsh8.6 "$0" "$@"

package require Tcl 8.6
package require tcltest 2.3

::tcltest::configure -testdir \
    [file join [file dirname [file normalize [info script]]] test]
::tcltest::configure {*}$argv
::tcltest::runAllTests
