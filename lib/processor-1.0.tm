############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2021 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require struct::list 1.8
package require struct::set 2.2

package require patterns 1.0
package require shops 1.0

package provide processor 1.0

namespace eval processor {

    proc applySplitCategories {patterns splits} {
        struct::list mapfor split $splits {
            if {![dict get $split manual]} {
                set text [dict get $split text]
                if {[dict exists $split pattern]} {
                    set pat [dict get $split pattern]
                } else {
                    set pat ""
                }
                if {$pat ne "" && [patterns::isPatternExists $patterns $pat]} {
                    # don't reset existing pattern
                    set split
                } else {
                    lassign [patterns::getCategory $text $patterns] cat pat
                    dict set split category $cat
                    dict set split pattern $pat
                }
            } else {
                dict set split pattern  ""
            }
        }
    }

    proc applyBillRules {patterns shops bill} {
        set splits [dict get $bill splits]
        set splits [applySplitCategories $patterns $splits]
        dict set bill shopName [::shops::getShopNameByTin $shops [dict get $bill TIN] [dict get $bill shopName]]
        dict set bill splits $splits
    }

    proc applyRules {patterns shops billList} {
        struct::list map $billList [list applyBillRules $patterns $shops]
    }

    proc listUnmatched {billList} {
        set res {}
        foreach bill $billList {
            foreach split [dict get $bill splits] {
                if {[dict get $split category] eq ""} {
                    lappend res [dict get $split text]
                }
            }
        }
        return $res
    }

    proc listCategories {billList} {
        set res {}
        foreach bill $billList {
            foreach split [dict get $bill splits] {
                set category [dict get $split category]
                if {$category ne ""} {
                    struct::set include res $category
                }
            }
        }
        return $res
    }

    namespace export *

}

# vim: set ft=tcl:

