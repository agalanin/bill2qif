package require Tcl 8.6
package require tcltest 2.3

::tcltest::configure {*}$argv
::tcl::tm::path add [file join [file dirname [file normalize [info script]]] .. lib]

package require processor

namespace eval ::processor::test {
    namespace import ::tcltest::*

    source [file join [file dirname [info script]] common.tcl]

    customMatch dictList ::test::match::matchDictLists
    customMatch bill ::test::match::matchBills
    customMatch multipleBills ::test::match::matchMultipleBills

    test apply-all-categories "All categories were applied" -match dictList -body {
        processor::applySplitCategories {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "CORN*"     Food
            "*POTATO*"  Food
        } {
            {text "CAT LITTER"      sum 35500  manual false}
            {text "CABBAGE"         sum  3979  manual false}
            {text "CORNCOB"         sum 10260  manual false}
            {text "RED POTATOES"    sum  2282  manual false}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER*}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern CABBAGE*}
            {text "CORNCOB"         sum 10260  manual false    category Food   pattern CORN*}
            {text "RED POTATOES"    sum  2282  manual false    category Food   pattern *POTATO*}
    }

    test apply-all-categories-manual "All categories were applied (with manual entries)" -match dictList -body {
        processor::applySplitCategories {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "CORN*"     Food
            "*POTATO*"  Food
        } {
            {text "CAT LITTER"      sum 35500  manual false}
            {text "CABBAGE"         sum  3979  manual false}
            {text "CORNCOB"         sum 10260  manual false}
            {text "RED POTATOES"    sum  2282  manual true     category Souvenirs}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
            {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
            {text "CORNCOB"         sum 10260  manual false    category Food       pattern CORN*}
            {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern {}}
    }

    test apply-partial-categories "Not all categories were applied" -match dictList -body {
        processor::applySplitCategories {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "*POTATO*"  Food
        } {
            {text "CAT LITTER"      sum 35500  manual false}
            {text "CABBAGE"         sum  3979  manual false}
            {text "CORNCOB"         sum 10260  manual false}
            {text "RED POTATOES"    sum  2282  manual true     category Souvenirs}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
            {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
            {text "CORNCOB"         sum 10260  manual false    category {}         pattern ""}
            {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern ""}
    }

    test apply-categories-keep-old "Keep old category assignment if valid" -match dictList -body {
        processor::applySplitCategories {
            "LEMONADE*" Lemonade
            "LEMON*"    Food
        } {
            {text "LEMONADE 1"      sum 1234   manual false    category Lemonade   pattern LEMONADE*}
            {text "LEMONADE 2"      sum 5678   manual false    category Food       pattern LEMON*}
            {text "LEMONADE 3"      sum 9012   manual false    category Food       pattern *MONADE*}
        }
    } -result {
            {text "LEMONADE 1"      sum 1234   manual false    category Lemonade   pattern LEMONADE*}
            {text "LEMONADE 2"      sum 5678   manual false    category Food       pattern LEMON*}
            {text "LEMONADE 3"      sum 9012   manual false    category Lemonade   pattern LEMONADE*}
    }

    test apply-single-bill "Apply categories to a single bill" -match bill -body {
        processor::applyBillRules {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "*POTATO*"  Food
        } {
        } {
            dateTime    1505664780
            sum         166984
            shopName    "Horns'n'Hoofs Ltd"
            TIN         1234567890
            splits {
                {text "CAT LITTER"      sum 35500  manual false}
                {text "CABBAGE"         sum  3979  manual false}
                {text "CORNCOB"         sum 10260  manual false}
                {text "RED POTATOES"    sum  2282  manual true     category Souvenirs}
            }
        }
    } -result {
            dateTime    1505664780
            sum         166984
            shopName    "Horns'n'Hoofs Ltd"
            TIN         1234567890
            splits {
                {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
                {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
                {text "CORNCOB"         sum 10260  manual false    category ""         pattern ""}
                {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern ""}
            }
    }

    test apply-multiple-bills "Apply categories to list of bills" -match multipleBills -body {
        processor::applyRules {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "*POTATO*"  Food
            "LEMONADE*" Food
        } {
        } {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false}
                    {text "CABBAGE"         sum  3979  manual false}
                    {text "CORNCOB"         sum 10260  manual false}
                    {text "RED POTATOES"    sum  2282  manual true     category Souvenirs}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                TIN         2222222222
                splits {
                    {text "LEMONADE 1"      sum 4766   manual false}
                    {text "LEMONADE 2"      sum 4766   manual false}
                }
            }
        }
    } -result {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
                    {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
                    {text "CORNCOB"         sum 10260  manual false    category ""         pattern ""}
                    {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern ""}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                TIN         2222222222
                splits {
                    {text "LEMONADE 1"      sum 4766   manual false    category Food   pattern LEMONADE*}
                    {text "LEMONADE 2"      sum 4766   manual false    category Food   pattern LEMONADE*}
                }
            }
    }

    test all-categories-set "Check that all categories is set" {
        processor::listUnmatched {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                splits {
                    {text "CAT LITTER"      sum 35500  category Cat        manual false}
                    {text "CABBAGE"         sum  3979  category Food       manual false}
                    {text "CORNCOB"         sum 10260  category Food       manual false}
                    {text "RED POTATOES"    sum  2282  category Souvenirs  manual true}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                splits {
                    {text "LEMONADE 1" sum 4766 category Food manual false}
                    {text "LEMONADE 2" sum 4766 category Food manual false}
                }
            }
        }
    } {}

    test not-all-categories-set "Check that all categories is set (failed)" {
        processor::listUnmatched {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                splits {
                    {text "CAT LITTER"      sum 35500  category Cat        manual false}
                    {text "CABBAGE"         sum  3979  category Food       manual false}
                    {text "CORNCOB"         sum 10260  category {}         manual false}
                    {text "RED POTATOES"    sum  2282  category Souvenirs  manual true}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                splits {
                    {text "LEMONADE 1" sum 4766 category Food  manual false}
                    {text "LEMONADE 2" sum 4766 category {}    manual false}
                }
            }
        }
    } {CORNCOB {LEMONADE 2}}

    test list-categories "List categories" {
        processor::listCategories {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                splits {
                    {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
                    {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
                    {text "CORNCOB"         sum 10260  manual false    category ""         pattern ""}
                    {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern ""}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                splits {
                    {text "LEMONADE 1"      sum 4766   manual false    category Food   pattern LEMONADE*}
                    {text "LEMONADE 2"      sum 4766   manual false    category Food   pattern LEMONADE*}
                }
            }
        }
    } {Cat Food Souvenirs}

    test apply-shop-rules "Apply shop name rules" -match multipleBills -body {
        processor::applyRules {
            "*LITTER*"  Cat
            "CABBAGE*"  Food
            "*POTATO*"  Food
            "LEMONADE*" Food
        } {
            1111111111 "Store 1"
        } {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false}
                    {text "CABBAGE"         sum  3979  manual false}
                    {text "CORNCOB"         sum 10260  manual false}
                    {text "RED POTATOES"    sum  2282  manual true     category Souvenirs}
                }
            }
            {
                dateTime    1505664780
                sum         166984
                shopName    "Horns'n'Hoofs Ltd"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                TIN         2222222222
                splits {
                    {text "LEMONADE 1"      sum 4766   manual false}
                    {text "LEMONADE 2"      sum 4766   manual false}
                }
            }
        }
    } -result {
            {
                dateTime    1505664780
                sum         166984
                shopName    "Store 1"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
                    {text "CABBAGE"         sum  3979  manual false    category Food       pattern CABBAGE*}
                    {text "CORNCOB"         sum 10260  manual false    category ""         pattern ""}
                    {text "RED POTATOES"    sum  2282  manual true     category Souvenirs  pattern ""}
                }
            }
            {
                dateTime    1505664780
                sum         166984
                shopName    "Store 1"
                TIN         1111111111
                splits {
                    {text "CAT LITTER"      sum 35500  manual false    category Cat        pattern *LITTER*}
                }
            }
            {
                dateTime    1493258280
                sum         9532
                shopName    "STORE 2"
                TIN         2222222222
                splits {
                    {text "LEMONADE 1"      sum 4766   manual false    category Food   pattern LEMONADE*}
                    {text "LEMONADE 2"      sum 4766   manual false    category Food   pattern LEMONADE*}
                }
            }
    }

    cleanupTests
}

namespace delete ::processor::test

# vim: set ft=tcl:
