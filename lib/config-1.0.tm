############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2021 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require inifile 0.3
package require msgcat

package require patterns 1.0
package require shops 1.0

package provide config 1.0

namespace eval config {
    namespace import ::msgcat::mc

    variable versionMajor 1
    variable versionMinor 0

    proc readFromFile {fileName} {
        variable versionMajor

        set ini [::ini::open $fileName r]
        try {
            if {![ini::exists $ini config format_version]} {
                throw {BILL2QIF LOGIC} [mc "unsupported format version"]
            }
            set version [::ini::value $ini config format_version]
            lassign [split $version .] major minor
            if {$major ne $versionMajor} {
                throw {BILL2QIF LOGIC} [mc "unsupported format version"]
            }

            set patterns [::patterns::readFromFile $ini]
            set shops [::shops::readFromFile $ini]
        } finally {
            ::ini::close $ini
        }

        list $patterns $shops
    }

    proc saveToFile {fileName patterns shops} {
        variable versionMajor
        variable versionMinor

        if {![file exists $fileName]} {
            # create file if not exists
            set f [open $fileName w]
            close $f
        }
        set ini [::ini::open $fileName r+]
        try {
            if {![ini::exists $ini config format_version]} {
                ::ini::set $ini config format_version $versionMajor.$versionMinor
            }
            ::shops::saveToFile $ini $shops
            ::patterns::saveToFile $ini $patterns

            ::ini::commit $ini
        } finally {
            ::ini::close $ini
        }
    }

    namespace export *

}

# vim: set ft=tcl:
