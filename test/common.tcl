proc dataFile {fname} {
    ::tcltest::viewFile $fname [file join [file dirname [info script]] data]
}

namespace eval ::test {}
namespace eval ::test::match {

proc matchDictLists {expected actual} {
    if {[llength $expected] != [llength $actual]} {
        return false
    }
    foreach a $expected b $actual {
        if {[dict size $a] != [dict size $b]} {
            return false
        }
        dict for {key value} $a {
            if {![dict exists $b $key]} {
                return false
            }
            if {$value ne [dict get $b $key]} {
                return false
            }
        }
    }
    return true
}

proc matchBills {expected actual} {
    if {[dict size $expected] != [dict size $actual]} {
        return false
    }
    dict for {key value} $expected {
        if {![dict exists $actual $key]} {
            return false
        }
        if {$key eq "splits"} {
            if {![matchDictLists $value [dict get $actual $key]]} {
                return false
            }
        } elseif {$value ne [dict get $actual $key]} {
            return false
        }
    }
    return true
}

proc matchMultipleBills {expected actual} {
    if {[llength $expected] != [llength $actual]} {
        return false
    }
    foreach a $expected b $actual {
        if {![matchBills $a $b]} {
            return false
        }
    }
    return true
}

}
