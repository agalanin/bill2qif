############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2021 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require struct::set
package require inifile 0.3

package provide shops 1.0

namespace eval shops {

    proc getShopNameByTin {shops tin name} {
        if {[dict exists $shops $tin]} {
            return [dict get $shops $tin]
        } else {
            return $name
        }
    }

    proc setShopName {shopsVar tin name} {
        upvar $shopsVar shops

        dict set shops $tin $name
    }

    proc saveToFile {ini shops} {
        ::ini::delete $ini shops

        dict for {shop category} $shops {
            ::ini::set $ini shops $shop $category
        }
    }

    proc readFromFile {ini} {
        if {[::ini::exists $ini shops]} {
            return [::ini::get $ini shops]
        } else {
            return {}
        }
    }

    namespace export *

}

# vim: set ft=tcl:
