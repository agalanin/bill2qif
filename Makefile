DEST=bill2qif
VERSION=$(shell grep -E "variable version [^ ]+" bill2qif.tcl | awk '{print $$3}')
DEST_EXE=$(DEST)-$(VERSION).exe
BUILDDIR=build

prefix=/usr/local
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
libdir=$(exec_prefix)/lib
datarootdir=$(prefix)/share
datadir=$(datarootdir)/$(DEST)
msgsdir=$(datadir)/msgs
docdir=$(datarootdir)/doc/$(DEST)
INSTALL=install
INSTALL_PROGRAM=$(INSTALL)
INSTALL_DATA=$(INSTALL) -m 644
HG=hg
HG_ARCHIVAL=.hg_archival.txt

TCLKITSH=./tclkitsh-8.5.9-linux-ix86
TCLKIT_WIN32=tclkit-gui-8_6_8-x86.exe
SDX=sdx-20110317.kit
TCLLIB=/usr/share/tcltk/tcllib1.19
TKLIB=/usr/share/tcltk/tklib0.6

ifeq "$(realpath $(HG_ARCHIVAL))" ""
latesttag=$(shell $(HG) parents --template {latesttag})
latesttagdistance=$(shell $(HG) parents --template {latesttagdistance})
else
latesttag=$(shell grep ^latesttag: $(HG_ARCHIVAL) | cut -d ' ' -f 2)
latesttagdistance=$(shell grep ^latesttagdistance: $(HG_ARCHIVAL) | cut -d ' ' -f 2)
endif

all:

install:
	mkdir -p "$(DESTDIR)$(bindir)/"
	$(INSTALL_PROGRAM) "$(DEST).tcl" "$(DESTDIR)$(bindir)/$(DEST)"
	mkdir -p "$(DESTDIR)$(libdir)/$(DEST)"
	$(INSTALL_DATA) lib/*.tm "$(DESTDIR)$(libdir)/$(DEST)"
	mkdir -p "$(DESTDIR)$(msgsdir)/"
	$(INSTALL_DATA) msgs/*.msg "$(DESTDIR)$(msgsdir)"
	mkdir -p "$(DESTDIR)$(docdir)/"
	$(INSTALL_DATA) README.md "$(DESTDIR)$(docdir)"

uninstall:
	rm "$(DESTDIR)$(bindir)/$(DEST)"
	rm -r "$(DESTDIR)$(libdir)/$(DEST)/"
	rm -r "$(DESTDIR)$(datadir)/"
	rm -r "$(DESTDIR)$(docdir)/"

starpack: $(DEST_EXE)

$(DEST_EXE):
	mkdir -p $(BUILDDIR)
	cp $(DEST).tcl $(BUILDDIR)/main.tcl
	mkdir -p $(BUILDDIR)/tm
	cp -r lib/*.tm $(BUILDDIR)/tm
	mkdir -p $(BUILDDIR)/lib
	cp -r $(TCLLIB)/cmdline/    $(BUILDDIR)/lib
	cp -r $(TCLLIB)/control/    $(BUILDDIR)/lib
	cp -r $(TCLLIB)/json/       $(BUILDDIR)/lib
	cp -r $(TCLLIB)/struct/     $(BUILDDIR)/lib
	cp -r $(TCLLIB)/inifile/    $(BUILDDIR)/lib
	cp -r $(TKLIB)/autoscroll/  $(BUILDDIR)/lib
	cp -r $(TKLIB)/tablelist/   $(BUILDDIR)/lib
	mkdir -p $(BUILDDIR)/msgs
	cp -r msgs/*.msg $(BUILDDIR)/msgs

	$(TCLKITSH) $(SDX) wrap $(DEST_EXE) -runtime $(TCLKIT_WIN32) -vfs $(BUILDDIR)

tarball:
ifeq "$(realpath $(HG_ARCHIVAL))" ""
	$(HG) archive --type tgz $(DEST)-$(VERSION).tar.gz
else
	@printf "You're already have a tarball, isn't it?\n\n"
endif

distrib: tarball starpack
	@[ "$(latesttagdistance)" -eq 0 ] || (echo "version is not updated!"; exit 1)
	@[ "$(latesttag)" = "$(VERSION)" ] || (echo "version mismatch!"; exit 1)

clean:
	rm -f $(DEST)-*.tar.gz
	rm -f $(DEST)-*.exe
	rm -rf $(BUILDDIR)

.PHONY: all install uninstall starpack tarball distrib clean
