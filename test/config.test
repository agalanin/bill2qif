package require Tcl 8.6
package require tcltest 2.3
package require struct::list
package require msgcat

::tcltest::configure {*}$argv
::tcl::tm::path add [file join [file dirname [file normalize [info script]]] .. lib]

::msgcat::mclocale en

package require config

namespace eval ::config::test {
    namespace import ::tcltest::*

    proc outfile {data} {
        join [struct::list mapfor line [split [string trim $data] \n] {
            string trim $line
        }] \n
    }

    test saveToFile "Save config to file" {
        set f [file tempfile tmpFile]
        try {
            close $f

            ::config::saveToFile $tmpFile {
                foo*    Foo
                *bar*   Bar
            } {
                12345   Shop
                67890   Store
            }
            
            set data [string trim [viewFile $tmpFile]]
        } finally {
            file delete -force $tmpFile
        }

        set data
    } [outfile {
        [patterns]
        *bar*=Bar
        foo*=Foo

        [shops]
        12345=Shop
        67890=Store

        [config]
        format_version=1.0
    }]

    test readFromFile "Read config from file" {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=1.0

                [shops]
                12345=Shop
                67890=Store

                [patterns]
                foo*    = Foo
                *bar*   = Bar
            }
            close $f

            set data [::config::readFromFile $tmpFile]
        } finally {
            file delete -force $tmpFile
        }

        set data
    } {{*bar* Bar foo* Foo} {12345 Shop 67890 Store}}

    test readEmptyPattern "Read config from file (empty pattern is ignored)" {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=1.0

                [shops]
                12345=Shop
                67890=Store

                [patterns]
                = Foo
                *bar*   = Bar
            }
            close $f

            set data [::config::readFromFile $tmpFile]
        } finally {
            file delete -force $tmpFile
        }

        set data
    } {{*bar* Bar} {12345 Shop 67890 Store}}

    test readEmptyCategory "Read config from file (empty category)" {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=1.0

                [shops]
                12345=Shop
                67890=Store

                [patterns]
                foo*    = Foo
                *bar*   =
            }
            close $f

            set data [::config::readFromFile $tmpFile]
        } finally {
            file delete -force $tmpFile
        }

        set data
    } {{*bar* {} foo* Foo} {12345 Shop 67890 Store}}

    test readUnsupportedFormatVersion "Read config with unsupported format version" -body {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=2.0
            }
            close $f
            ::config::readFromFile $tmpFile
        } finally {
            file delete -force $tmpFile
        }
    } -returnCodes error -result {unsupported format version}

    test readNewerFormatVersion "Read config with newer but supported format version" {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=1.1

                [new]
                foo=bar

                [patterns]
                foo*    = Foo
                *bar*   = Bar
            }
            close $f
            set data [::config::readFromFile $tmpFile]
        } finally {
            file delete -force $tmpFile
        }

        set data
    } {{*bar* Bar foo* Foo} {}}

    test saveNewerFormatVersion "Save config into newer but supported format version" {
        set f [file tempfile tmpFile]
        try {
            puts $f {
                [config]
                format_version=1.1

                [new]
                foo=bar
            }
            close $f

            ::config::saveToFile $tmpFile {
                *foo*   Bar
            } {
                1234567890  "Store"
            }

            set data [viewFile $tmpFile]
        } finally {
            file delete -force $tmpFile
        }

        string trim [set data]
    } [outfile {
        [patterns]
        *foo*=Bar

        [shops]
        1234567890=Store

        [new]
        foo=bar

        [config]
        format_version=1.1
    }]

    cleanupTests
}

namespace delete ::config::test

# vim: set ft=tcl:
