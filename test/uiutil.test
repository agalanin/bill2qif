package require Tcl 8.6
package require tcltest 2.3

::tcltest::configure {*}$argv
::tcl::tm::path add [file join [file dirname [file normalize [info script]]] .. lib]

package require uiutil

namespace eval ::uiutil::test {
    namespace import ::tcltest::*

    customMatch listList [namespace current]::matchListList

    proc matchListList {expected actual} {
        if {[llength $expected] != [llength $actual]} {
            return false
        }
        foreach a $expected b $actual {
            if {[llength $a] != [llength $b]} {
                return false
            }
            foreach aa $a bb $b {
                if {$aa ne $bb} {
                    return false
                }
            }
        }
        return true
    }

    test splits-to-list-auto "Check split to list conversion (auto-category)" -match listList -body {
        uiutil::splitsToList {
            {text {CAT LITTER}      sum 35500 category Cat     pattern *LITTER     manual false    quantity 1       unit pcs}
            {text CABBAGE           sum  3979 category Food    pattern CABBAGE*    manual false    quantity 3.524   unit kg}
            {text CORNCOB           sum 10260 category Food    pattern CORN*       manual false    quantity 4}
            {text {RED POTATOES}    sum  2282 category Food    pattern *POTATO*    manual false    quantity 1.216}
        }
    } -result {
        {"CAT LITTER"   355.00 Cat  *LITTER     1       "pcs"}
        {"CABBAGE"       39.79 Food CABBAGE*    3.524   "kg"}
        {"CORNCOB"      102.60 Food CORN*       4       ""}
        {"RED POTATOES"  22.82 Food *POTATO*    1.216   ""}
    }

    test splits-to-list-manual "Check split to list conversion (manual category assignment)" -match listList -body {
        uiutil::splitsToList {
            {text {CAT LITTER}      sum 35500 category Cat     pattern *LITTER     manual false    quantity 1       unit pcs}
            {text CABBAGE           sum  3979 category Food    pattern CABBAGE*    manual false    quantity 3.524   unit kg}
            {text CORNCOB           sum 10260 category Food    pattern CORN*       manual false    quantity 4}
            {text {RED POTATOES}    sum  2282 category Gift    pattern ""          manual false    quantity 1.216}
        }
    } -result {
        {"CAT LITTER"   355.00 Cat  *LITTER     1       "pcs"}
        {"CABBAGE"       39.79 Food CABBAGE*    3.524   "kg"}
        {"CORNCOB"      102.60 Food CORN*       4       ""}
        {"RED POTATOES"  22.82 Gift {}          1.216   ""}
    }

    test list-to-splits-auto "Check list to split conversion (auto)" -match listList -body {
        uiutil::listToSplits {
            {"CAT LITTER"   355.00 Cat  *LITTER     1       "pcs"}
            {"CABBAGE"       39.79 Food CABBAGE*    3.524   "kg"}
            {"CORNCOB"      102.60 Food CORN*       4}
            {"RED POTATOES"  22.82 Food *POTATO*    1.216}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER     quantity 1       unit pcs}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern CABBAGE*    quantity 3.524   unit kg}
            {text "CORNCOB"         sum 10260  manual false    category Food   pattern CORN*       quantity 4}
            {text "RED POTATOES"    sum  2282  manual false    category Food   pattern "*POTATO*"  quantity 1.216}
    }

    test list-to-splits-manual "Check list to split conversion (manual)" -match listList -body {
        uiutil::listToSplits {
            {"CAT LITTER"   355.00 Cat  *LITTER     1       "pcs"}
            {"CABBAGE"       39.79 Food CABBAGE*    3.524   "kg"}
            {"CORNCOB"      102.60 Junk ""          4}
            {"RED POTATOES"  22.82 Gift ""          1.216}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER     quantity 1       unit pcs}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern CABBAGE*    quantity 3.524   unit kg}
            {text "CORNCOB"         sum 10260  manual true     category Junk   pattern ""          quantity 4}
            {text "RED POTATOES"    sum  2282  manual true     category Gift   pattern ""          quantity 1.216}
    }

    test list-to-splits-empty "Check list to split conversion (with empty categories)" -match listList -body {
        uiutil::listToSplits {
            {"CAT LITTER"   355.00 Cat  *LITTER     1}
            {"CABBAGE"       39.79 Food CABBAGE*    3.524}
            {"CORNCOB"      102.60 Junk ""          4}
            {"RED POTATOES"  22.82 "" ""            1.216}
        }
    } -result {
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER     quantity 1}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern CABBAGE*    quantity 3.524}
            {text "CORNCOB"         sum 10260  manual true     category Junk   pattern ""          quantity 4}
            {text "RED POTATOES"    sum  2282  manual false    category ""     pattern ""          quantity 1.216}
    }

    test uncategorized-list-start-ok "First uncategorized item in list (found)" {
        uiutil::firstUncategorizedInList {
            {"RED POTATOES"  22.82 "" ""            1}
            {"CAT LITTER"   355.00 Cat  *LITTER     3.524}
            {"CABBAGE"       39.79 Food ""          4}
            {"CORNCOB"      102.60 "" ""            1.216}
        } 1
    } {3}

    test uncategorized-list-start-nok "First uncategorized item in list (not found)" {
        uiutil::firstUncategorizedInList {
            {"RED POTATOES"  22.82 "" ""            1}
            {"CORNCOB"      102.60 "" ""            3.524}
            {"CAT LITTER"   355.00 Cat  *LITTER     4}
            {"CABBAGE"       39.79 Food CABBAGE*    1.216}
        } 2
    } {-1}

    test uncategorized-splits-start-ok "First uncategorized item in splits (found)" {
        uiutil::firstUncategorizedInSplits {
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER quantity 1}
            {text "RED POTATOES"    sum  2282  manual false    category Food   pattern ""      quantity 3.524}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern ""      quantity 4}
            {text "CORNCOB"         sum 10260  manual true     category ""     pattern ""      quantity 1.216}
        }
    } {3}

    test uncategorized-splits-start-nok "First uncategorized item in splits (not found)" {
        uiutil::firstUncategorizedInSplits {
            {text "RED POTATOES"    sum  2282  manual false    category Food   pattern ""          quantity 1}
            {text "CORNCOB"         sum 10260  manual true     category Food   pattern ""          quantity 3.524}
            {text "CAT LITTER"      sum 35500  manual false    category Cat    pattern *LITTER     quantity 4}
            {text "CABBAGE"         sum  3979  manual false    category Food   pattern CABBAGE*    quantity 1.216}
        }
    } {-1}

    cleanupTests
}

namespace delete ::uiutil::test

# vim: set ft=tcl:
