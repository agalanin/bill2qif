############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2022 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require json 1.3
package require struct::list 1.8
package require msgcat

package provide bills 1.0

namespace eval bills {
    namespace import ::msgcat::mc

    proc parseSplit {data dictName invert} {
        upvar $dictName splits

        set name [dict get $data name]
        if {![dict exists $splits $name]} {
            dict set splits $name [list \
                text        $name \
                sum         0 \
                quantity    0 \
                manual      false
            ]
        }
        dict with splits $name {
            set incr [expr {[dict get $data sum] + 0}]
            if {$invert} {
                set incr [expr {-$incr}]
            }
            set sum [expr {$sum + $incr}]
            set quantity [expr {$quantity + [dict get $data quantity]}]
        }
        if {[dict exists $data unit]} {
            dict set splits $name unit [dict get $data unit]
        }
    }

    proc parseBill {data} {
        # get time
        set dateTime [dict get $data dateTime]
        switch -regexp $dateTime {
            {^[0-9]+$} {
                # UNIX time

                # Decrement timeval to Europe/Moscow time offset. This offset is added
                # for unknown reason to both unixtime and ISO-like timestamp formats.
                set dateTime [clock add $dateTime -3 hours]
            }
            {^[0-9]{4,4}-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$} {
                # old time format
                set dateTime [clock scan $dateTime \
                    -format %Y-%m-%dT%H:%M:%S \
                    -gmt yes \
                ]
                # Decrement timeval to Europe/Moscow time offset. This offset is added
                # for unknown reason to both unixtime and ISO-like timestamp formats.
                set dateTime [clock add $dateTime -3 hours]
            }
            {^[0-9]{4,4}-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]\+[0-9][0-9]:[0-9][0-9]$} {
                # new time format
                set dateTime [clock scan $dateTime \
                    -format %Y-%m-%dT%H:%M:%S%Z \
                ]
            }
        }

        # detect type
        set typeId [dict get $data operationType]
        set type [lindex {"" Income "Income Refund" Outcome "Outcome Refund"} $typeId]
        if {$type eq ""} {
            set type [mc "Unknown(%d)" $typeId]
            throw {BILL2QIF LOGIC} [mc "bill type '%s' is not yet supported" $type]
        }
        set invert [expr {$type in {"Income Refund" "Outcome"}}]

        # parse shop name
        if {[dict exists $data retailPlace]} {
            set shopName [string trim [dict get $data retailPlace]]
        } else {
            set shopName ""
        }
        if {$shopName in {"" "null"} && [dict exists $data user]} {
            set shopName [string trim [dict get $data user]]
        }
        if {$shopName in {"" "null"}} {
            set shopName [string trim [dict get $data userInn]]
        }
        if {$shopName in {"" "null"}} {
            set shopName -
        }

        # parse markups and discounts
        set markupSum 0
        set discountSum 0
        set markupName [mc "Markup"]
        set discountName [mc "Discount"]
        if {[dict exists $data modifiers]} {
            foreach modifier [dict get $data modifiers] {
                if {[dict exists $modifier markupSum]} {
                    set sum [dict get $modifier markupSum]
                    if {$sum ni {"" "null"}} {
                        set markupSum [expr {$markupSum + $sum}]
                    }
                }
                if {[dict exists $modifier markupName]} {
                    set markupName [dict get $modifier markupName]
                }
                if {[dict exists $modifier discountSum]} {
                    set sum [dict get $modifier discountSum]
                    if {$sum ni {"" "null"}} {
                        set discountSum [expr {$discountSum + $sum}]
                    }
                }
                if {[dict exists $modifier discountName]} {
                    set discountName [dict get $modifier discountName]
                }
            }
        }


        # parse splits
        set splits {}
        set i 1
        foreach item [dict get $data items] {
            try {
                parseSplit $item splits $invert
            } on error {err opts} {
                dict set opts -errorinfo "split $i: [dict get $opts -errorinfo]"
                dict set opts -errorcode {BILL2QIF LOGIC}
                return -options $opts [mc "split %d: %s" $i $err]
            }
            incr i
        }
        set splitList [dict values $splits]

        # add markup and discount splits
        if {$markupSum != 0} {
            lappend splitList [list \
                text        $markupName \
                sum         [expr {$markupSum + 0}] \
                quantity    1 \
                manual      false
            ]
        }
        if {$discountSum != 0} {
            lappend splitList [list \
                text        $discountName \
                sum         [expr {-$discountSum + 0}] \
                quantity    1 \
                manual      false
            ]
        }

        # parse sum
        set sum 0
        if {[dict exists $data cashTotalSum]} {
            incr sum [dict get $data cashTotalSum]
        }
        if {[dict exists $data ecashTotalSum]} {
            incr sum [dict get $data ecashTotalSum]
        }
        set sum [expr {$sum + 0}]
        if {$invert} {
            set sum [expr {-$sum}]
        }

        list \
            dateTime    $dateTime \
            sum         $sum \
            shopName    $shopName \
            TIN         [string trim [dict get $data userInn]] \
            splits      $splitList
    }

    proc parseData {json} {
        if {[catch {set data [json::json2dict $json]} err opts]} {
            error $err $::errorInfo {BILL2QIF INPUTFORMAT}
        }
        if {[llength $data] == 0} {
            throw {BILL2QIF LOGIC} [mc "empty bill list"]
        }

        if {[llength $data] % 2 == 0 && [dict exists $data totalSum]} {
            # looks like a single-bill file
            try {
                set res [list [parseBill $data]]
            } on error {err opts} {
                dict set opts -errorcode {BILL2QIF LOGIC}
                return -options $opts $err
            }
            return $res
        } else {
            # parse as multiple-bill file
            set res {}
            set i 1
            foreach doc $data {
                try {
                    if {[dict exists $doc document]} {
                        # variant 1: list of document->receipt->bill structures
                        set bill [dict get $doc document receipt]
                    } elseif {[dict exist $doc items]} {
                        # variant 2: list of bill structures
                        set bill $doc
                    } elseif {[dict exists $doc ticket document receipt]} {
                        # variant 3: list of ticket->document->receipt structures
                        set bill [dict get $doc ticket document receipt]
                    } elseif {[dict exists $doc ticket document bso]} {
                        # variant 4: list of ticket->document->bso structures
                        set bill [dict get $doc ticket document bso]
                    } else {
                        throw {BILL2QIF LOGIC} [mc "unsupported bill format"]
                    }
                    lappend res [parseBill $bill]
                } on error {err opts} {
                    dict set opts -errorinfo "bill $i: [dict get $opts -errorinfo]"
                    dict set opts -errorcode {BILL2QIF LOGIC}
                    return -options $opts [mc "bill %d: %s" $i $err]
                }
                incr i
            }
            return $res
        }
    }

    proc validate {bills} {
        set i 1
        set msg {}
        foreach bill $bills {
            set totalSum [dict get $bill sum]
            set splitSum [struct::list fold [dict get $bill splits] 0 {apply {{sum split} {
                expr {$sum + [dict get $split sum]}
            }}}]

            set diff [expr {$totalSum - $splitSum}]
            # ignore short change rounding (less than 0.1 RUB)
            if {abs($diff) > 10} {
                lappend msg [mc "bill %d: %s" $i \
                    [mc "total sum %.2f differs with split sum %.2f" \
                        [expr {$totalSum / 100.0}] \
                        [expr {$splitSum / 100.0}] \
                    ] \
                ]
            }
            incr i
        }
        join $msg \n
    }

    namespace export *

}

# vim: set ft=tcl:

