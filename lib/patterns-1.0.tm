############################################################################
# bill2qif - an utility to convert receipts in Russian Federal Tax Service
# format into Quicken Interchange Format (QIF)
# Copyright (C) 2018-2021 Alexander Galanin <al@galanin.nnov.ru>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
############################################################################

package require Tcl 8.6
package require struct::set
package require inifile 0.3

package provide patterns 1.0

namespace eval patterns {

    proc isMatches {pattern string} {
        string match -nocase $pattern $string
    }

    proc findConflictingPattern {pattern patterns {ignoreDups false}} {
        dict for {p cat} $patterns {
            if {$ignoreDups && $p eq $pattern} {
                continue
            }
            if {[isMatches $pattern $p] || [isMatches $p $pattern]} {
                return [list $p $cat]
            }
        }
        return
    }

    proc validateList {patterns} {
        set all {}
        foreach {pattern category} $patterns {
            set p [string toupper $pattern]
            if {[::struct::set contains $all $p]} {
                return [list duplicate $pattern]
            } elseif {$category eq ""} {
                return [list empty $pattern]
            } else {
                ::struct::set add all $p
            }
        }

        dict for {pattern category} $patterns {
            set res [findConflictingPattern $pattern $patterns true]
            if {[llength $res] > 0} {
                return [list conflict $pattern $category {*}$res]
            }
        }
        return
    }

    proc getCategory {text patterns} {
        dict for {pattern category} $patterns {
            if {[isMatches $pattern $text]} {
                return [list $category $pattern]
            }
        }
        return
    }

    proc isPatternExists {patterns pattern} {
        dict exists $patterns $pattern
    }

    proc saveToFile {ini patterns} {
        ::ini::delete $ini patterns

        # force section creation
        ::ini::set $ini patterns dummy fake
        ::ini::delete $ini patterns dummy

        dict for {pattern category} $patterns {
            ::ini::set $ini patterns $pattern $category
        }
    }

    proc readFromFile {ini} {
        if {[::ini::exists $ini patterns]} {
            return [::ini::get $ini patterns]
        } else {
            return {}
        }
    }

    namespace export *

}

# vim: set ft=tcl:
